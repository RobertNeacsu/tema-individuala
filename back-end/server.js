const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,varsta VARCHAR(3),cnp VARCHAR(20),sex VARCHAR(3))";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let error = []

    if (!nume||!prenume||!telefon||!email||!facebook||!nrCard||!varsta||!cnp){
        error.push("Unul sau mai multe campuri nu au fost introduse");
        console.log("Unul sau mai multe campuri nu au fost introduse");}
    else{
        if(nume.length < 3 || nume.length > 20){
            error.push("Nume invalid!");
            console.log("Nume invalid!");
        }
        else
            if(!nume.match("^[A-Za-z]+$")){
                error.push("Numele trebuie sa contina doar litere!");
                console.log("Numele trebuie sa contina doar litere!");
            }
    
        if(prenume.length < 3 || prenume.length > 20){
            error.push("Prenume invalid");
            console.log("Prenume invalid!");
        }
        else
            if(!prenume.match("^[A-Za-z]+$")){
                error.push("Prenumele trebuie sa contina doar litere!");
                console.log("Prenumele trebuie sa contina doar numere!");
            }
        if(telefon.length != 10){
            error.push("Numarul de telefon invalid!");
            console.log("Numar de telefon invalid!");
        }
        else
            if(!telefon.match("^[0-9]+$")){
                error.push("Numar de telefon invalid");
                console.log("Numar de telefon invalid");
            }else
                if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {
                    console.log("Email invalid!");
                    error.push("Email invalid!");
              }else
                    if (!facebook.includes("https://")){
                        console.log("Link Facebook invalid!");
                        error.push("Link Facebok invalid!");
                } 
                    else
                        if(nrCard.length!=16 || !nrCard.match("^[0-9]+$")){
                            error.push("Numar card invalid!");
                            console.log("Numar card invalid!");
                        } 
                        else
                            if(cvv.length!=3 || !cvv.match("^[0-9]+$")){
                                error.push("CVV invalid");
                                console.log("CVV invalid!");
                            } 
                            else
                                if(varsta.length < 1 || varsta.length > 3 || !varsta.match("^[0-9]+$")){
                                    error.push("Varsta invalida!");
                                    console.log("Varsta invalida!");
                                }
                                else 
                                    if(cnp.length != 13 || !cnp.match("^[0-9]+$")){
                                        error.push("CNP invalid!");
                                        console.log("CNP invalid");
                                    }
                               if((cnp.slice(1,3)+varsta)%100 != 19){
                                   error.push("CNP/varsta invalid!");
                                   console.log("CNP/varsta invalid!");
                               }
                               if(cnp.slice(0,1).match("1") || cnp.slice(0,1).match("3") || cnp.slice(0,1).match("5"))
                                sex="M";
                            else
                               sex="F";
                         }
                          if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,varsta,cnp,sex) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" + cvv + "','" +varsta +"','" +cnp +"','" +sex+ "')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }

});